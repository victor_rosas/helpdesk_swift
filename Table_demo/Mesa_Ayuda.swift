//
//  Mesa_Ayuda.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 08/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class Mesa_Ayuda: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var myTable: UITableView!
    @IBOutlet weak var spiner: UIActivityIndicatorView!
    @IBOutlet weak var btn_parametros: UIBarButtonItem!
    @IBOutlet weak var btn_clientes: UIBarButtonItem!
    var arrayMesaAyuda = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        spiner.startAnimating()
        btn_parametros.enabled = false
        btn_clientes.enabled = false
        self.traerMesa()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //Tabla
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMesaAyuda.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: CustomCell_mesa = tableView.dequeueReusableCellWithIdentifier("Cell_mesa") as CustomCell_mesa
        let mesa = arrayMesaAyuda[indexPath.row] as Mesa_principal
        
        cell.setCell(mesa.sNombre, label_sHoraLlegada: mesa.sHoraLlegada.stringValue, label_sHoraSalida: mesa.sHoraSalida.stringValue, label_sMesActual: mesa.sMesActual.stringValue)
        return cell
    }
    
    func traerMesa()
    {
        DataManager.traeMesaAyuda { (mesaData) -> Void in
            let json = JSON(data: mesaData)
            
            if let mesaArray = json["traeMesaAyudaResult"].arrayValue {
                
                for mesaDict in mesaArray {
                    
                    var nIdUsuario: NSNumber? = mesaDict["nIdUsuario"].numberValue
                    var sNombre: String? = mesaDict["sNombre"].stringValue
                    var sHoraLlegada: String? = mesaDict["sHoraLlegada"].stringValue
                    var sHoraSalida: NSString? = mesaDict["sHoraSalida"].stringValue
                    var sDiaActual: NSString? = mesaDict["sDiaActual"].stringValue
                    var sSemanaActual: NSString? = mesaDict["sSemanaActual"].stringValue
                    var sMesActual: NSString? = mesaDict["sMesActual"].stringValue
                    
                    var mes_numero = sMesActual?.floatValue
                    var semana_numero = sSemanaActual?.floatValue
                    var dia_numero = sDiaActual?.floatValue
                    
                    var mesa = Mesa_principal(nIdUsuario: nIdUsuario!, sNombre: sNombre, sHoraLlegada: dia_numero, sHoraSalida: semana_numero, sMesActual: mes_numero)
                    
                    self.arrayMesaAyuda.addObject(mesa)
                    
                }
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.myTable.reloadData()
                    self.btn_parametros.enabled = true
                    self.btn_clientes.enabled = true
                    self.spiner.stopAnimating()
                })
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "Tecnico")
        {
            let detail_view_controller:Mesa_Parametros = segue.destinationViewController as Mesa_Parametros
            for i in 0..<arrayMesaAyuda.count
            {
                detail_view_controller.arrayMesa.addObject(arrayMesaAyuda[i])
            }
            
        }
        
    }
    
}
    


