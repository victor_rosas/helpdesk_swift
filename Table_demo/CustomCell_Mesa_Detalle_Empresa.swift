//
//  CustomCell_Mesa_Detalle_Empresa.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 10/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class CustomCell_Mesa_Detalle_Empresa: UITableViewCell {
    @IBOutlet weak var label_tarea: UILabel!
    @IBOutlet weak var label_inicio: UILabel!
    @IBOutlet weak var label_final: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(tarea: String, inicio: String, final: String)
    {
        self.label_tarea.text = tarea
        self.label_inicio.text = inicio
        self.label_final.text = final
    }

}
