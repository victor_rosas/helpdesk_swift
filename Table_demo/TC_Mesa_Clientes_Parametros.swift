//
//  TC_Mesa_Clientes_Parametros.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 11/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class TC_Mesa_Clientes_Parametros: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet weak var label_fecha_inicio: UILabel!
    @IBOutlet weak var label_fecha_final: UILabel!
    @IBOutlet weak var label_cliente: UILabel!
    @IBOutlet weak var picker_cliente: UIPickerView!
    @IBOutlet weak var picker_fecha: UIDatePicker!

    let arrayCliente = NSMutableArray()
    let formato_fecha = NSDateFormatter()
    let formato_fecha_string = NSDateFormatter()
    var fecha_inicio = NSDate()
    var fecha_final = NSDate()
    var cliente = "Ninguno"
    var selected_row = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formato_fecha.dateFormat = "dd-MM-yyyy"
        formato_fecha_string.dateFormat = "dd/MM/yyyy"
        label_fecha_inicio.text = formato_fecha.stringFromDate(fecha_inicio)
        label_fecha_final.text = formato_fecha.stringFromDate(fecha_final)
         self.view.bringSubviewToFront(picker_cliente)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.traerClientes()
        var ip = NSIndexPath(forRow: 0, inSection: 0)
        self.tableView.selectRowAtIndexPath(ip, animated: true, scrollPosition: UITableViewScrollPosition.Bottom)
        selected_row=0
        picker_fecha.hidden = false
        picker_cliente.hidden = true
        picker_fecha.setDate(fecha_inicio, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func date_picked(sender: AnyObject) {
        escribir_fecha(sender.date!!)
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayCliente.count+1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if(row == 0)
        {
            return "Ninguno"
        }
        var persona = arrayCliente[row-1] as String
        return persona
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(row == 0)
        {
            label_cliente.text = "Ninguno"
            cliente = "Ninguno"
        }
        else
        {
            var persona = arrayCliente[row-1] as String
            label_cliente.text = persona
            cliente = persona
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selected_row = indexPath.row
        if(selected_row == 2)
        {
            picker_fecha.hidden = true
            picker_cliente.hidden = false
        }
        else
        {
            picker_fecha.hidden = false
            picker_cliente.hidden = true
            if(selected_row == 0)
            {
                picker_fecha.setDate(fecha_inicio, animated: true)
            }
            else
            {
                picker_fecha.setDate(fecha_final, animated: true)
            }
        }
    }
    
    func escribir_fecha(date:NSDate)
    {
        if(selected_row == 0)
        {
            fecha_inicio = date
            label_fecha_inicio.text = formato_fecha.stringFromDate(fecha_inicio)
        }
        else if(selected_row == 1)
        {
            fecha_final = date
            label_fecha_final.text = formato_fecha.stringFromDate(fecha_final)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let detail_view_controller:VC_Mesa_Clientes_1 = segue.destinationViewController as VC_Mesa_Clientes_1
        
        detail_view_controller.idTecnico = "0"
        detail_view_controller.fecha_inicio = formato_fecha_string.stringFromDate(fecha_inicio)
        detail_view_controller.fecha_final = formato_fecha_string.stringFromDate(fecha_final)
        detail_view_controller.cliente = cliente
        
    }

    @IBAction func btn_home_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func traerClientes()
    {
        DataManager.traeClientes { (mesaData) -> Void in
            let json = JSON(data: mesaData)
            
            if let mesaArray = json["traeClientesResult"].arrayValue {
                
                for mesaDict in mesaArray {
                    
                    var sCliente: String? = mesaDict["sCliente"].stringValue
                    self.arrayCliente.addObject(sCliente!)
                }
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.picker_cliente.reloadAllComponents()
                })
            }
        }
    }
}
