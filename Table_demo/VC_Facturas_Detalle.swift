//
//  VC_Facturas_Detalle.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 10/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class VC_Facturas_Detalle: UIViewController {
    var total: NSNumber = 0
    var costo: NSNumber = 0
    var utilidad: NSNumber = 0
    
    var factura: NSNumber = 0
    var tipo: NSNumber = 0
    var almacen: NSNumber = 0
    var renglon: NSNumber = 0
    var caja: NSNumber = 0
    
    @IBOutlet weak var label_total: UITextField!
    @IBOutlet weak var label_costo: UITextField!
    @IBOutlet weak var label_utilidad: UITextField!
    @IBOutlet weak var candado: UISwitch!
    
    
    let formato_moneda = NSNumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formato_moneda.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        formato_moneda.locale = NSLocale(localeIdentifier: "es_MX")
        self.setup()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup()
    {
        label_total.text = self.formato_moneda.stringFromNumber(total)
        label_total.text = label_total.text?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        label_costo.text = costo.stringValue
        utilidad = total.integerValue - costo.integerValue
        
        label_utilidad.text = self.formato_moneda.stringFromNumber(utilidad)
        label_utilidad.text = label_utilidad.text?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    @IBAction func costo_changed(sender: AnyObject) {
        var costo_label = label_costo.text.toInt()
        utilidad = total.integerValue - costo_label!
        label_utilidad.text = self.formato_moneda.stringFromNumber(utilidad)
        label_utilidad.text = label_utilidad.text?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    @IBAction func btn_guardar_pressed(sender: AnyObject) {
        label_costo.resignFirstResponder()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btn_back_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func bnt_modificacion(sender: AnyObject) {
        var lock: String = ""
        if(candado.on)
        {
            lock = "true"
        }
        else
        {
            lock = "false"
        }
        DataManager.establecerModificacionDetalle(self.factura.stringValue, tipo: self.tipo.stringValue, almacen: self.almacen.stringValue, caja: self.caja.stringValue, renglon: self.renglon.stringValue, costo: label_costo.text, bloqueo: lock)
        DataManager.modificarFacturasDetalle { (facturaData) -> Void in
            let json = JSON(data: facturaData)
            
            if let facturaArray = json["ModificaCostoResult"].arrayValue {
                
                for facturaDic in facturaArray {
                    var cambio_realizado: Bool = facturaDic["bModificado"].boolValue
                    
                    if(cambio_realizado)
                    {
                        var alertController = UIAlertController(title: "", message: "Cambio realizado exitosamente.", preferredStyle: .Alert)
                        var okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        alertController.addAction(okAction)
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                    else
                    {
                        let alert = UIAlertView()
                        alert.title = ""
                        alert.message = "No se pudo realizar el cambio."
                        alert.addButtonWithTitle("Ok")
                        alert.show()
                    }
                }
            }
        }
    }
}
