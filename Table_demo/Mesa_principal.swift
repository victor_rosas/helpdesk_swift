//
//  Mesa_principal.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 08/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import Foundation

class Mesa_principal: NSObject {
    let nIdUsuario: NSNumber
    let sNombre: String
    let sHoraLlegada: NSNumber
    let sHoraSalida: NSNumber
    let sMesActual: NSNumber
    
    
    init(nIdUsuario: NSNumber, sNombre: String?, sHoraLlegada: NSNumber?, sHoraSalida: NSNumber?, sMesActual: NSNumber?){
        
        
        self.nIdUsuario = nIdUsuario ?? 0
        self.sNombre = sNombre ?? ""
        self.sHoraLlegada = sHoraLlegada ?? 0
        self.sHoraSalida = sHoraSalida ?? 0
        self.sMesActual = sMesActual ?? 0
        
    }
}