//
//  CustomCell_mesa.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 08/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class CustomCell_mesa: UITableViewCell {
    @IBOutlet weak var sNombre: UILabel!
    @IBOutlet weak var sHoraLlegada: UILabel!
    @IBOutlet weak var sHoraSalida: UILabel!
    @IBOutlet weak var sMesActual: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setCell(label_sNombre: String, label_sHoraLlegada: String, label_sHoraSalida: String, label_sMesActual: String)
    {
        self.sHoraLlegada.layer.cornerRadius = 5
        self.sHoraLlegada.layer.masksToBounds = true
        self.sHoraSalida.layer.cornerRadius = 5
        self.sHoraSalida.layer.masksToBounds = true
        
        self.sNombre.text = " " + label_sNombre
        self.sHoraLlegada.text = label_sHoraLlegada
        self.sHoraSalida.text = label_sHoraSalida
        self.sMesActual.text = label_sMesActual
    }

}
