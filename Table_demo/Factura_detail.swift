//
//  Factura_detail.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 12/11/14.
//  Copyright (c) 2014 Isaac. All rights reserved.
//

import UIKit

class Factura_detail: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var nIdAlmacen: NSNumber = 0
    var nIdTipo: NSNumber = 0
    var nIdMovimiento: NSNumber = 0
    var nIdCaja: NSNumber = 0
    
    var total_factura: NSNumber = 0
    
    var formato = NSNumberFormatter()
    
    @IBOutlet weak var myTable: UITableView!
    var arrayFacturas = [Factura_detalles]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        formato.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        formato.locale = NSLocale(localeIdentifier: "en_US")
        self.traeDetFactura()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayFacturas.count+1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if(indexPath.row == arrayFacturas.count)
        {
            let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell") as UITableViewCell
            
            cell.textLabel.textAlignment = NSTextAlignment.Right
            cell.textLabel.text = self.formato.stringFromNumber(total_factura)?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            return cell
        }
        else
        {
            let cell: CustomCell_Detalles = tableView.dequeueReusableCellWithIdentifier("cell") as CustomCell_Detalles
            
            let factura = arrayFacturas[indexPath.row]
            cell.setCell(factura.sArticulo, dCantidad: factura.dCantidad.stringValue, dImporte: factura.dImporte, dIVA: factura.dIVA, dTotalDet: factura.dTotalDet)
            return cell
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func back_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func traeDetFactura()
    {
        DataManager.establecerBusqueda(nIdAlmacen, Tipo: nIdTipo, Movimiento: nIdMovimiento, Caja: nIdCaja)
        
        DataManager.traeDetFactura { (facturaData) -> Void in
            let json = JSON(data: facturaData)
            
            if let facturasArray = json["traeDetFacturaResult"].arrayValue {
                
                for facturasDict in facturasArray {
                    
                    var dCantidad: NSNumber? = facturasDict["dCantidad"].numberValue
                    var sArticulo: String? = facturasDict["sArticulo"].stringValue
                    var dImporte: NSNumber? = facturasDict["dImporte"].numberValue
                    var dIVA: NSNumber? = facturasDict["dIva"].numberValue
                    var dTotalDet: NSNumber? = facturasDict["dTotalDet"].numberValue
                    var dTotal: NSNumber? = facturasDict["dTotal"].numberValue
                    
                    self.total_factura = dTotal!
                    
                    var dImporte_formato = self.formato.stringFromNumber(dImporte!)?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    
                    var dIVA_formato = self.formato.stringFromNumber(dIVA!)?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    
                    var dTotalDet_formato = self.formato.stringFromNumber(dTotalDet!)?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    
                    var factura = Factura_detalles(dCantidad: dCantidad!, sArticulo: sArticulo, dImporte: dImporte_formato!, dIVA: dIVA_formato!, dTotalDet: dTotalDet_formato!)
                    
                    self.arrayFacturas.append(factura)
                }
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.myTable.reloadData()
                })
            }
        }
        
    }

}
