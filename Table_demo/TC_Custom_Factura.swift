//
//  TC_Custom_Factura.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 10/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class TC_Custom_Factura: UITableViewCell {
    @IBOutlet weak var label_factura: UILabel!
    @IBOutlet weak var label_cliente: UILabel!
    @IBOutlet weak var label_costo: UILabel!
    @IBOutlet weak var label_utilidad: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(label_factura: String, label_cliente: String, label_fecha: String, label_costo: String)
    {
        self.label_costo.layer.cornerRadius = 5
        self.label_costo.layer.masksToBounds = true
        
        self.label_factura.text = label_factura
        self.label_cliente.text = " " + label_cliente
        self.label_costo.text = label_fecha
        self.label_utilidad.text = label_costo
    }

}
