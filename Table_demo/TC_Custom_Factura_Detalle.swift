//
//  TC_Custom_Factura_Detalle.swift
//  Table_demo
//
//  Created by Miguel Banderas on 15/01/15.
//  Copyright (c) 2015 Aaron. All rights reserved.
//

import UIKit

class TC_Custom_Factura_Detalle: UITableViewCell {

    
    @IBOutlet weak var label_sArticulo: UILabel!
    @IBOutlet weak var label_nPrecioUnitario: UILabel!
    @IBOutlet weak var label_nCantidad: UILabel!
    @IBOutlet weak var label_nTotal: UILabel!
    @IBOutlet weak var label_nCosto: UILabel!
    @IBOutlet weak var label_nUtilidad: UILabel!
    @IBOutlet weak var icono_candado: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(label_sArticulo: String, label_nPrecioUnitario: String, label_nCantidad: String, label_nTotal: String, label_nCosto:String, label_nUtilidad: String, candado: Bool)
    {
        self.label_sArticulo.text = " " + label_sArticulo
        self.label_nCantidad.text = "x" + label_nCantidad
        self.label_nPrecioUnitario.text = label_nPrecioUnitario
        self.label_nTotal.text = label_nTotal
        self.label_nCosto.text = "- " + label_nCosto
        self.label_nUtilidad.text = label_nUtilidad
        
        if(candado)
        {
            icono_candado.hidden = false
        }
        else
        {
            icono_candado.hidden = true
        }
    }

}
