//
//  Objeto_Mesa_Ayuda_Detalles.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 09/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class Objeto_Mesa_Ayuda_Detalles: NSObject {
    let sTecnico: String
    let sCliente: String
    let sTituloTicket: String
    let sFechaInicio: NSDate
    let sFechaFin: NSDate
    let sFecha: String
    let sMinutos: Int
   
    init(sTecnico: String, sCliente: String?, sTituloTicket: String?, sFechaInicio: NSDate, sFechaFin: NSDate, sFecha: String?, sMinutos: Int){
        
        self.sTecnico = sTecnico ?? ""
        self.sCliente = sCliente ?? ""
        self.sTituloTicket = sTituloTicket ?? ""
        self.sFechaInicio = sFechaInicio
        self.sFechaFin = sFechaFin
        self.sFecha = sFecha ?? ""
        self.sMinutos = sMinutos
    }
}
