//
//  TC_Mesa_Ayuda_Detalles.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 11/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class TC_Mesa_Ayuda_Detalles: UITableViewCell {
    @IBOutlet weak var label_empresa: UILabel!
    @IBOutlet weak var label_horas: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setCell(Empresa: String, horas: String)
    {
        self.label_empresa.text = Empresa
        self.label_horas.text = horas
        
    }

}
