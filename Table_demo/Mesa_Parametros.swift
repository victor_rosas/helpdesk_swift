//
//  Mesa_Parametros.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 08/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class Mesa_Parametros: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet weak var label_fecha_termino: UILabel!
    @IBOutlet weak var label_fecha_inicio: UILabel!
    @IBOutlet weak var label_tecnico: UILabel!
    @IBOutlet weak var pickerView: UIDatePicker!
    @IBOutlet weak var pickerTecnicoView: UIPickerView!
    
    let arrayMesa = NSMutableArray()
    let formato_fecha = NSDateFormatter()
    let formato_fecha_string = NSDateFormatter()
    var fecha_inicio = NSDate()
    var fecha_final = NSDate()
    var id_persona : NSNumber = 0
    var selected_row = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        formato_fecha.dateFormat = "dd-MM-yyyy"
        formato_fecha_string.dateFormat = "dd/MM/yyyy"
        
        label_fecha_inicio.text = formato_fecha.stringFromDate(fecha_inicio)
        label_fecha_termino.text = formato_fecha.stringFromDate(fecha_final)
        
        
        self.view.bringSubviewToFront(pickerTecnicoView)
    }
    
    @IBAction func back_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //Seleccionar la primera fila de la tabla automaticamente
    override func viewWillAppear(animated: Bool) {
        var ip = NSIndexPath(forRow: 0, inSection: 0)
        self.tableView.selectRowAtIndexPath(ip, animated: true, scrollPosition: UITableViewScrollPosition.Bottom)
        selected_row=0
        pickerView.hidden = false
        pickerTecnicoView.hidden = true
        pickerView.setDate(fecha_inicio, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func date_picker_action(sender: AnyObject) {
        escribir_fecha(sender.date!!)
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMesa.count+1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if(row == 0)
        {
            return "Ninguno"
        }
        var persona = arrayMesa[row-1] as Mesa_principal
        return persona.sNombre
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(row == 0)
        {
            label_tecnico.text = "Ninguno"
            id_persona = 0
        }
        else
        {
            var persona = arrayMesa[row-1] as Mesa_principal
            label_tecnico.text = persona.sNombre
            id_persona = persona.nIdUsuario
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selected_row = indexPath.row
        if(selected_row == 2)
        {
            pickerView.hidden = true
            pickerTecnicoView.hidden = false
        }
        else
        {
            pickerView.hidden = false
            pickerTecnicoView.hidden = true
            if(selected_row == 0)
            {
                pickerView.setDate(fecha_inicio, animated: true)
            }
            else
            {
                pickerView.setDate(fecha_final, animated: true)
            }
        }
    }
    
    func escribir_fecha(date:NSDate)
    {
        if(selected_row == 0)
        {
            fecha_inicio = date
            label_fecha_inicio.text = formato_fecha.stringFromDate(fecha_inicio)
        }
        else if(selected_row == 1)
        {
            fecha_final = date
            label_fecha_termino.text = formato_fecha.stringFromDate(fecha_final)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let detail_view_controller:Mesa_Ayuda_Detalles = segue.destinationViewController as Mesa_Ayuda_Detalles
        
        detail_view_controller.idTecnico = id_persona.stringValue
        detail_view_controller.fecha_inicio = formato_fecha_string.stringFromDate(fecha_inicio)
        detail_view_controller.fecha_final = formato_fecha_string.stringFromDate(fecha_final)

    }

}
