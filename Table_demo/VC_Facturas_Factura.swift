//
//  VC_Facturas_Factura.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 10/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class VC_Facturas_Factura: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var fecha_inicio: String = ""
    var fecha_final: String = ""
    var arrayFacturas = NSMutableArray()
    var totalFacturas = 0
    var sortedArray = NSArray()
    
    @IBOutlet weak var myTableView: UITableView!
    let formato_fecha = NSDateFormatter()
    let formato_fecha_a_string = NSDateFormatter()
    let formato_moneda = NSNumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formato_fecha.locale = NSLocale(localeIdentifier: "es_US")
        formato_fecha.dateFormat = "MM'-'dd'-'yyyy hh:mm:ss a"
        formato_fecha_a_string.dateFormat = "dd-MM-yyyy"
        formato_moneda.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        formato_moneda.locale = NSLocale(localeIdentifier: "es_MX")
        self.traeFactura()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Setup de la tabla de facturas
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedArray.count+1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.row == sortedArray.count)
        {
            let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell_Facturas_Total") as UITableViewCell
            cell.textLabel.textAlignment = NSTextAlignment.Right
            cell.textLabel.text = self.formato_moneda.stringFromNumber(totalFacturas)?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            return cell
        }
        else
        {
            let cell: TC_Custom_Factura = tableView.dequeueReusableCellWithIdentifier("Cell_Facturas_Factura") as TC_Custom_Factura
            let factura = sortedArray[indexPath.row] as OBJ_Factura
            var fecha_formato = formato_fecha_a_string.stringFromDate(factura.sFecha)
            cell.setCell(factura.nFactura.stringValue, label_cliente: factura.sCliente, label_fecha: fecha_formato, label_costo: factura.nTotalFactura)
            return cell
        }
    }
    
    func traeFactura(){
        DataManager.establecerBusquedaFactura(fecha_inicio, fechaFinal: fecha_final, Almacen: "2")
        DataManager.traerFacturas { (facturaData) -> Void in
            let json = JSON(data: facturaData)
            
            if let facturaArray = json["ConsultaFacturasResult"].arrayValue {
                
                for facturaDic in facturaArray {
                    var nAlmacen: NSNumber = facturaDic["nAlmacen"].numberValue!
                    var nCaja: NSNumber = facturaDic["nCaja"].numberValue!
                    var nFactura: NSNumber = facturaDic["nFactura"].numberValue!
                    var nTipo: NSNumber = facturaDic["nTipo"].numberValue!
                    var nTotalFactura: NSNumber = facturaDic["nTotalFactura"].numberValue!
                    var sCliente: String = facturaDic["sCliente"].stringValue!
                    var sFecha: String = facturaDic["sFecha"].stringValue!
                    
                    var fechaFormato = self.formato_fecha.dateFromString(sFecha)
                    var costoFormato = self.formato_moneda.stringFromNumber(nTotalFactura)
                    costoFormato = costoFormato?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    
                    var factura = OBJ_Factura(nAlmacen: nAlmacen, nCaja: nCaja, nFactura: nFactura, nTipo: nTipo, nTotalFactura: costoFormato!, sCliente: sCliente, sFecha: fechaFormato!)
                    self.totalFacturas += nTotalFactura.integerValue
                    self.arrayFacturas.addObject(factura)
                }
                
                var sorter = NSSortDescriptor(key: "sFecha", ascending: false)
                self.sortedArray = (self.arrayFacturas as NSArray).sortedArrayUsingDescriptors([sorter])
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.myTableView.reloadData()
                })
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let siguiente:VC_Facturas_Desglose = segue.destinationViewController as VC_Facturas_Desglose
        var factura : OBJ_Factura
        let row = myTableView.indexPathForSelectedRow()!.row
        factura = sortedArray[row] as OBJ_Factura
        
        siguiente.title = factura.nFactura.stringValue
        siguiente.factura = factura.nFactura
        siguiente.tipo = factura.nTipo
        siguiente.almacen = factura.nAlmacen
        siguiente.caja = factura.nCaja
        siguiente.fecha = self.formato_fecha_a_string.stringFromDate(factura.sFecha)
    }

    @IBAction func btn_back_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
