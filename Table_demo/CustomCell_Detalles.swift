//
//  CustomCell_Detalles.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 25/11/14.
//  Copyright (c) 2014 Isaac. All rights reserved.
//

import UIKit

class CustomCell_Detalles: UITableViewCell {
    @IBOutlet weak var sArticulo: UILabel!
    @IBOutlet weak var dCantidad: UILabel!
    @IBOutlet weak var dImporte: UILabel!
    @IBOutlet weak var dIVA: UILabel!
    @IBOutlet weak var dTotalDet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(sArticulo: String, dCantidad: String, dImporte: String, dIVA: String, dTotalDet: String)
    {
        self.sArticulo.text = sArticulo
        self.dCantidad.text = dCantidad
        self.dImporte.text = dImporte
        self.dIVA.text = dIVA
        self.dTotalDet.text = dTotalDet
    }

}
