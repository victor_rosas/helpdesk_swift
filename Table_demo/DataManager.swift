//
//  DataManager.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 24/11/14.
//  Copyright (c) 2014 Isaac. All rights reserved.
//

import Foundation
//Antiguedad
var almacen = ""
var tipo = ""
var movimiento = ""
var caja = ""
//Mesa de ayuda
var Tecnico = ""
var Fecha_inicio = ""
var Fecha_fin = ""
//Factura
var factura_fechaInicio = ""
var factura_fechaFinal = ""
var factura_almacen = ""
//Factura Detalle
var factura_factura = ""
var factura_tipoFactura = ""
var factura_detalle = ""
var factura_caja = ""
//Modificacion detalle
var factura_modificacion_factura = ""
var factura_modificacion_almacen = ""
var factura_modificacion_caja = ""
var factura_modificacion_tipo = ""
var factura_modificacion_renglon = ""
var factura_modificacion_costo = ""
var factura_modificacion_bloqueo = ""

class DataManager {
    
    class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
        var session = NSURLSession.sharedSession()
        
        // Use NSURLSession to get data from an NSURL
        let loadDataTask = session.dataTaskWithURL(url, completionHandler: { (data: NSData!, response: NSURLResponse!, error: NSError!) -> Void in
            if let responseError = error {
                completion(data: nil, error: responseError)
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    var statusError = NSError(domain:"com.raywenderlich", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                } else {
                    completion(data: data, error: nil)
                }
            }
        })
        
        loadDataTask.resume()
    }
    
    class func establecerBusqueda(Almacen: NSNumber, Tipo: NSNumber, Movimiento: NSNumber, Caja: NSNumber)
    {
        almacen = Almacen.stringValue
        tipo = Tipo.stringValue
        movimiento = Movimiento.stringValue
        caja = Caja.stringValue
    }
    
    class func traeSaldos(success: ((facturaData: NSData!) -> Void)) {
        //1
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let str_date = dateFormatter.stringFromDate(date)
        let Antiguedad_de_saldos = "http://173.192.80.226/WCFReportesADS/Service1.svc/json/traeSaldos?Fecha="+str_date
        
        loadDataFromURL(NSURL(string: Antiguedad_de_saldos)!, completion:{(data, error) -> Void in
            //2
            if let urlData = data {
                //3
                success(facturaData: urlData)
            }
        })
    }
    
    class func traeDetFactura(success: ((facturaData: NSData!) -> Void)) {
        //1
        var Trae_det_factura = "http://173.192.80.226/WCFReportesADS/Service1.svc/json/traeDetFactura?almacen=" + almacen + "&tipo=" + tipo + "&movimiento=" + movimiento + "&caja=" + caja
        loadDataFromURL(NSURL(string: Trae_det_factura)!, completion:{(data, error) -> Void in
            //2
            if let urlData = data {
                //3
                success(facturaData: urlData)
            }
        })
    }
    
    class func traeMesaAyuda (success: ((facturaData: NSData!) -> Void)) {
        //1
        var Trae_mesa = "http://173.192.80.226/WCFReportesADS/Service1.svc/json/traeMesaAyuda"
        loadDataFromURL(NSURL(string: Trae_mesa)!, completion:{(data, error) -> Void in
            //2
            if let urlData = data {
                //3
                success(facturaData: urlData)
            }
        })
    }
    
    class func establecerBusquedaMesa(idTecnico: String, inicio: String, fin: String)
    {
        Tecnico = idTecnico
        Fecha_inicio = inicio
        Fecha_fin = fin
    }
    
    class func traeMesaAyudaDetalles (success: ((facturaData: NSData!) -> Void)) {
        //1
        var Trae_mesa = "http://173.192.80.226/WCFReportesADS/Service1.svc/json/traeBusquedaMesaAyuda?IdTecnico=" + Tecnico + "&FechaInicio=" + Fecha_inicio + "&FechaFin=" + Fecha_fin
        loadDataFromURL(NSURL(string: Trae_mesa)!, completion:{(data, error) -> Void in
            //2
            if let urlData = data {
                //3
                success(facturaData: urlData)
            }
        })
    }
    
    class func traeClientes (success: ((facturaData: NSData!) -> Void)) {
        //1
        var Trae_mesa = "http://173.192.80.226/WCFReportesADS/Service1.svc/json/traeClientes"
        loadDataFromURL(NSURL(string: Trae_mesa)!, completion:{(data, error) -> Void in
            //2
            if let urlData = data {
                //3
                success(facturaData: urlData)
            }
        })
    }
    
    class func establecerBusquedaFactura (fechaInicio: String, fechaFinal: String, Almacen: String){
        factura_fechaInicio = fechaInicio
        factura_fechaFinal = fechaFinal
        factura_almacen = Almacen
    }
    
    
    class func traerFacturas (success: ((facturaData: NSData!) -> Void)) {
        //1
        var trae_factura = "http://173.192.80.226/WCFReportesADS/Service1.svc/json/ConsultaFacturas?FechaInicio=" + factura_fechaInicio + "&FechaFin=" + factura_fechaFinal + "&Almacen=" + factura_almacen
        loadDataFromURL(NSURL(string: trae_factura)!, completion:{(data, error) -> Void in
            //2
            if let urlData = data {
                //3
                success(facturaData: urlData)
            }
        })
    }
    
    class func establecerBusquedaFacturaDetalle (factura: String, tipo: String, almacen: String, caja: String){
        factura_factura = factura
        factura_tipoFactura = tipo
        factura_almacen = almacen
        factura_caja = caja
    }
    
    class func traerFacturasDetalle (success: ((facturaData: NSData!) -> Void)) {
        //1
        var trae_factura = "http://173.192.80.226/WCFReportesADS/Service1.svc/json/BuscaDetalleFactura?Factura=" + factura_factura + "&TipoFactura=" + factura_tipoFactura + "&Almacen=" + factura_almacen + "&Caja=" + factura_caja
        loadDataFromURL(NSURL(string: trae_factura)!, completion:{(data, error) -> Void in
            //2
            if let urlData = data {
                //3
                success(facturaData: urlData)
            }
        })
    }
    
    //MODIFICACIONES POR JSON
    class func establecerModificacionDetalle (factura: String, tipo: String, almacen: String, caja: String, renglon: String, costo: String, bloqueo: String){
        factura_modificacion_factura = factura
        factura_modificacion_tipo = tipo
        factura_modificacion_almacen = almacen
        factura_modificacion_caja = caja
        factura_modificacion_renglon = renglon
        factura_modificacion_costo = costo
        factura_modificacion_bloqueo = bloqueo
    }
    
    class func modificarFacturasDetalle (success: ((facturaData: NSData!) -> Void)) {
        //1
        var trae_factura = "http://173.192.80.226/WCFReportesADS/Service1.svc/json/ModificaCosto?Factura=" + factura_modificacion_factura + "&Almacen=" + factura_modificacion_almacen + "&Caja=" + factura_modificacion_caja + "&Tipo=" + factura_modificacion_tipo + "&Renglon=" + factura_modificacion_renglon + "&NuevoCosto=" + factura_modificacion_costo + "&Bloquear=" + factura_modificacion_bloqueo
        loadDataFromURL(NSURL(string: trae_factura)!, completion:{(data, error) -> Void in
            //2
            if let urlData = data {
                //3
                success(facturaData: urlData)
            }
        })
    }

}