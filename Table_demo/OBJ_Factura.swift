//
//  OBJ_Factura.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 10/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class OBJ_Factura: NSObject {
    let nAlmacen : NSNumber
    let nCaja: NSNumber
    let nFactura: NSNumber
    let nTipo: NSNumber
    let nTotalFactura: String
    let sCliente: String
    let sFecha: NSDate
    
    init(nAlmacen: NSNumber, nCaja: NSNumber, nFactura: NSNumber, nTipo: NSNumber, nTotalFactura: String, sCliente: String, sFecha: NSDate) {
        self.nAlmacen = nAlmacen ?? 0
        self.nCaja = nCaja ?? 0
        self.nFactura = nFactura ?? 0
        self.nTipo = nTipo ?? 0
        self.nTotalFactura = nTotalFactura ?? ""
        self.sCliente = sCliente ?? ""
        self.sFecha = sFecha
    }
    
}
