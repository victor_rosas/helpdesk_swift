//
//  Factura.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 12/11/14.
//  Copyright (c) 2014 Isaac. All rights reserved.
//

import Foundation

class Factura: NSObject {
    let sNumCte: String
    let sNombre: String
    let sReferencia: String
    let sFechaMovimiento: NSDate
    let dVen: String
    let dVen_number: NSNumber
    let dDias: NSNumber
    
    let nIdAlmacen: NSNumber
    let nIdTipo: NSNumber
    let nIdMovimiento: NSNumber
    let nIdCaja: NSNumber
    

    
    init(sNumCte: String?, sNombre: String?, sReferencia: String?, sFechaMovimiento: NSDate?, dVen: String, dVen_Number: NSNumber, dDias: NSNumber, nIdAlmacen: NSNumber, nIdTipo: NSNumber, nIdMovimiento: NSNumber, nIdCaja: NSNumber) {
        
        self.sNumCte = sNumCte ?? ""
        self.sNombre = sNombre ?? ""
        self.sReferencia = sReferencia ?? ""
        self.sFechaMovimiento = sFechaMovimiento!
        self.dVen = dVen ?? ""
        self.dVen_number = dVen_Number ?? 0
        self.dDias = dDias ?? 0
        
        self.nIdAlmacen = nIdAlmacen ?? 0
        self.nIdTipo = nIdTipo ?? 0
        self.nIdMovimiento = nIdMovimiento ?? 0
        self.nIdCaja = nIdCaja ?? 0
    }
    
}
