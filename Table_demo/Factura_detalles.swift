//
//  Factura_detalles.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 25/11/14.
//  Copyright (c) 2014 Isaac. All rights reserved.
//

import Foundation

class Factura_detalles {
    let dCantidad: NSNumber
    let sArticulo: String
    let dImporte: String
    let dIVA: String
    let dTotalDet: String
    
    init(dCantidad: NSNumber, sArticulo: String?, dImporte: String?, dIVA: String?, dTotalDet: String?)
    {
        self.dCantidad = dCantidad ?? 0
        self.sArticulo = sArticulo ?? ""
        self.dImporte = dImporte ?? ""
        self.dIVA = dIVA ?? ""
        self.dTotalDet = dTotalDet ?? ""
    }
    
}



