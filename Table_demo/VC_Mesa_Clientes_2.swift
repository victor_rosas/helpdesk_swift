//
//  VC_Mesa_Clientes_2.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 12/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class VC_Mesa_Clientes_2: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var arrayMesa = NSMutableArray()
    var sorted_arrayMesa = NSArray()
    var diccionario = NSMutableDictionary()
    var titulos = [String]()
    let formato_fecha = NSDateFormatter()
    @IBOutlet weak var myTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formato_fecha.dateFormat = "HH:mm"
        self.creacion_diccionario()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func creacion_diccionario()
    {
        
        for i in 0..<arrayMesa.count
        {
            var arrMesa = [Objeto_Mesa_Ayuda_Detalles]()
            var objeto = arrayMesa[i] as Objeto_Mesa_Ayuda_Detalles
            
            if(self.diccionario.objectForKey(objeto.sFecha)==nil)
            {
                arrMesa.append(objeto)
                self.diccionario.setObject(arrMesa, forKey: objeto.sFecha)
            }
            else
            {
                var arrTemporal = self.diccionario.objectForKey(objeto.sFecha) as [Objeto_Mesa_Ayuda_Detalles]
                arrTemporal.append(objeto)
                self.diccionario.setObject(arrTemporal, forKey: objeto.sFecha)
            }
        }
        
        self.titulos = self.diccionario.allKeys as [String]
        sort(&titulos)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.titulos.count
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView();
        header.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0)
        header.backgroundColor = UIColor(red: 130/255, green: 171/255, blue: 198/255, alpha: 1.0)
        let label = UILabel();
        label.frame = CGRect(x: 2, y: 1, width: tableView.frame.size.width, height: 20)
        label.text = titulos[section]
        label.font = UIFont(name: "Arial", size: 13)
        label.textColor = UIColor.whiteColor()
        header.addSubview(label)
        return header
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var llave = titulos[section]
        var arr = diccionario.objectForKey(llave) as [Objeto_Mesa_Ayuda_Detalles]
        return arr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let llave = titulos[indexPath.section]
        let arr = diccionario.objectForKey(llave) as [Objeto_Mesa_Ayuda_Detalles]
        
        
        var sorter = NSSortDescriptor(key: "sFechaInicio", ascending: true)
        var sorted = (arr as NSArray).sortedArrayUsingDescriptors([sorter])
        let mesa = sorted[indexPath.row] as Objeto_Mesa_Ayuda_Detalles
        
        
        let cell: TC_Mesa_Clientes_Detalles = tableView.dequeueReusableCellWithIdentifier("Cell_Mesa_Clientes_Detalle") as TC_Mesa_Clientes_Detalles
        var inicio = formato_fecha.stringFromDate(mesa.sFechaInicio)
        var final = formato_fecha.stringFromDate(mesa.sFechaFin)
        cell.setCell(mesa.sTituloTicket, inicio: inicio, final: final)
        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btn_home_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
