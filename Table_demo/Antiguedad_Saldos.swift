//
//  ViewController.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 12/11/14.
//  Copyright (c) 2014 Isaac. All rights reserved.
//

import UIKit

class Antiguedad_Saldos: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var myTableView: UITableView!
    var arrayFacturas = NSMutableArray()
    var sortedArray = NSArray()
    var totalFacturas = 0
    let formato = NSNumberFormatter()
    let date_now = NSDate()
    let formato_string_a_fecha = NSDateFormatter()
    let formato_fecha_a_string = NSDateFormatter()
    var bool_secciones = false
    //variables para secciones
    var diccionarioFacturas = NSMutableDictionary()
    var titulos = [String]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        formato.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        formato.locale = NSLocale(localeIdentifier: "es_MX")
        formato_string_a_fecha.locale = NSLocale(localeIdentifier: "es_US")
        formato_string_a_fecha.dateFormat = "MM'-'dd'-'yyyy hh:mm:ss a"//Cambios al formato de la fecha fecha
        formato_fecha_a_string.dateFormat = "dd-MM-yyyy"
        self.traerSaldos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(self.bool_secciones)
        {
            return titulos.count
        }
        else
        {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(self.bool_secciones)
        {
            let header = UIView();
            header.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0)
            header.backgroundColor = UIColor(red: 130/255, green: 171/255, blue: 198/255, alpha: 1.0)
            let label = UILabel();
            label.frame = CGRect(x: 2, y: 1, width: tableView.frame.size.width, height: 20)
            label.text = titulos[section]
            label.font = UIFont(name: "Arial", size: 12.2)
            label.textColor = UIColor.whiteColor()
            header.addSubview(label)
            return header
        }
        else
        {
            return nil
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.bool_secciones)
        {
            var llave = titulos[section]
            var arrFactura = diccionarioFacturas.objectForKey(llave) as [Factura]
            return arrFactura.count+1
        }
        else
        {
            return sortedArray.count+1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        if(self.bool_secciones)
        {
            let llave = titulos[indexPath.section]
            let arrFactura = diccionarioFacturas.objectForKey(llave) as [Factura]
            if(indexPath.row == arrFactura.count)
            {
                var contador = 0;
                var total = 0
                
                while(contador < arrFactura.count)
                {
                    total += Int(arrFactura[contador].dVen_number)
                    contador++
                }
                
                let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
                cell.textLabel.textAlignment = NSTextAlignment.Right
                
                cell.textLabel.text = self.formato.stringFromNumber(total)?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                
                return cell
            }
            else
            {
                let factura = arrFactura[indexPath.row]
                let cell: CustomCell = tableView.dequeueReusableCellWithIdentifier("Cell") as CustomCell
                
                //var fecha_cambio = factura.sFechaMovimiento.stringByReplacingOccurrencesOfString("/", withString: "-")
                //var fecha = formato_string_a_fecha.dateFromString(fecha_cambio)
                var fecha_formato = formato_fecha_a_string.stringFromDate(factura.sFechaMovimiento)
                
                cell.setCell(factura.sReferencia, label_dVen: factura.dVen, label_dDias: factura.dDias.stringValue, label_sFechaMovimiento: fecha_formato, label_sNombre: "", label_show: true)
                return cell
            }
            
        }
        else
        {
            if(indexPath.row == sortedArray.count)
            {
                let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
                cell.textLabel.textAlignment = NSTextAlignment.Right
                
                cell.textLabel.text = self.formato.stringFromNumber(totalFacturas)?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                
                return cell
            }
            else
            {
                let cell: CustomCell = tableView.dequeueReusableCellWithIdentifier("Cell") as CustomCell
                let factura = sortedArray[indexPath.row] as Factura

                //var fecha_cambio = factura.sFechaMovimiento.stringByReplacingOccurrencesOfString("/", withString: "-")
                //var fecha = formato_string_a_fecha.dateFromString(fecha_cambio)
                var fecha_formato = formato_fecha_a_string.stringFromDate(factura.sFechaMovimiento)
                cell.setCell(factura.sReferencia, label_dVen: factura.dVen, label_dDias: factura.dDias.stringValue, label_sFechaMovimiento: fecha_formato, label_sNombre: factura.sNombre, label_show: false)
                return cell
            }
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(self.bool_secciones)
        {
            let llave = titulos[indexPath.section]
            let arrFactura = diccionarioFacturas.objectForKey(llave) as [Factura]
            if(indexPath.row == arrFactura.count)
            {
                return 40
            }
            else
            {
                return 40
            }
        }
        else
        {
            if(indexPath.row == sortedArray.count)
            {
                return 40
            }
            else
            {
                return 60
            }
        }
    }
    
    //en caso de no tener segues se puede usar esto!
    
    /*func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let factura = arrayFacturas[indexPath.row]
        var detailViewController : Factura_detail = self.storyboard?.instantiateViewControllerWithIdentifier("DetallesFactura") as Factura_detail
        
        detailViewController.number = factura.Number
        detailViewController.name = factura.Name
        detailViewController.descripcion = factura.Descripcion
        
        self.presentViewController(detailViewController, animated: true, completion: nil)
        self.myTableView.deselectRowAtIndexPath(indexPath, animated: false)
    }*/
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let detail_view_controller:Factura_detail = segue.destinationViewController as Factura_detail
        var factura : Factura
        if(self.bool_secciones)
        {
            let section = myTableView.indexPathForSelectedRow()!.section
            let row = myTableView.indexPathForSelectedRow()!.row
            let llave = titulos[section]
            let arrFactura = diccionarioFacturas.objectForKey(llave) as [Factura]
            factura = arrFactura[row] as Factura
        }
        else
        {
            let row = myTableView.indexPathForSelectedRow()!.row
            factura = sortedArray[row] as Factura
        }
        
        detail_view_controller.title = factura.sReferencia
        detail_view_controller.nIdAlmacen = factura.nIdAlmacen
        detail_view_controller.nIdTipo = factura.nIdTipo
        detail_view_controller.nIdMovimiento = factura.nIdMovimiento
        detail_view_controller.nIdCaja = factura.nIdCaja
    }
    
    
    @IBAction func btn_back_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btn_secciones_pressed(sender: AnyObject) {
        self.bool_secciones = !self.bool_secciones
        myTableView.reloadData();
    }
    
    
    func traerSaldos()
    {
        DataManager.traeSaldos { (facturaData) -> Void in
            let json = JSON(data: facturaData)
            
            if let facturasArray = json["traeSaldosResult"].arrayValue {
                
                for facturasDict in facturasArray {
                    
                    //Validacion
                    var dDias: NSNumber? = facturasDict["nDias"].numberValue
                    //if(dDias>0)
                    //{
                        var sNumCte: String = facturasDict["sNumCte"].stringValue!
                        var sNombre: String? = facturasDict["sNombre"].stringValue
                        var sReferencia: String? = facturasDict["nIdMovimiento"].stringValue
                        var sFechaMovimiento: String = facturasDict["sFechaMovimiento"].stringValue!
                    
                        //Dven, ya no se usa por ahora
                        //var dVen1: NSNumber? = facturasDict["dVen1"].numberValue
                        //var dVen2: NSNumber? = facturasDict["dVen2"].numberValue
                        //var dVen3: NSNumber? = facturasDict["dVen3"].numberValue
                        //var dVen4: NSNumber? = facturasDict["dVen4"].numberValue
                        //var dVen5: NSNumber? = facturasDict["dVen5"].numberValue
                        //var dVenReal = dVen1!.integerValue + dVen2!.integerValue + dVen3!.integerValue + dVen4!.integerValue + dVen5!.integerValue
                    
                        var nIdAlmacen: NSNumber? = facturasDict["nIdAlmacen"].numberValue
                        var nidTipo: NSNumber? = facturasDict["nIdTipo"].numberValue
                        var nIdMovimiento: NSNumber? = facturasDict["nIdMovimiento"].numberValue
                        var nIdCaja: NSNumber? = facturasDict["nIdCaja"].numberValue
                        
                        var dVenReal : NSNumber = facturasDict["dTotal"].numberValue!
                
                        var dVen_formato = self.formato.stringFromNumber(dVenReal)
                        dVen_formato = dVen_formato?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    
                    
                        var fecha_cambio = sFechaMovimiento.stringByReplacingOccurrencesOfString("/", withString: "-")
                        var fecha = self.formato_string_a_fecha.dateFromString(fecha_cambio)
                    
                        let calendar = NSCalendar.currentCalendar()
                        let datecomponenets = calendar.components(NSCalendarUnit.CalendarUnitDay, fromDate: self.date_now, toDate: fecha!, options: nil)
                        let days = -1*(datecomponenets.day)
                    
                        var factura = Factura(sNumCte: sNumCte, sNombre: sNombre, sReferencia: sReferencia, sFechaMovimiento: fecha, dVen: dVen_formato!, dVen_Number: dVenReal, dDias: days, nIdAlmacen: nIdAlmacen!, nIdTipo: nidTipo!, nIdMovimiento: nIdMovimiento!, nIdCaja: nIdCaja!)
                        
                        
                            var arrFactura = [Factura]()
                            if(self.diccionarioFacturas.objectForKey(sNumCte)==nil)
                            {
                                arrFactura.append(factura)
                                self.diccionarioFacturas.setObject(arrFactura, forKey: sNumCte)
                            }
                            else
                            {
                                var arrTemporal = self.diccionarioFacturas.objectForKey(sNumCte) as [Factura]
                                arrTemporal.append(factura)
                                self.diccionarioFacturas.setObject(arrTemporal, forKey: sNumCte)
                            }
                        
                        
                        self.totalFacturas += dVenReal.integerValue
                        self.arrayFacturas.addObject(factura)
                    //}
                    
                }
                
                
                var sorter = NSSortDescriptor(key: "sFechaMovimiento", ascending: true)
                self.sortedArray = (self.arrayFacturas as NSArray).sortedArrayUsingDescriptors([sorter])
                
                self.titulos = self.diccionarioFacturas.allKeys as [String]
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.myTableView.reloadData()
                })
            }
        }
        
    }

}

