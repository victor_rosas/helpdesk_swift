//
//  OBJ_Factura_Detalle.swift
//  Table_demo
//
//  Created by Miguel Banderas on 15/01/15.
//  Copyright (c) 2015 Aaron. All rights reserved.
//

import UIKit

class OBJ_Factura_Detalle: NSObject {
    let bBloqueCosto : Bool
    let nAlmacen : NSNumber
    let nCaja: NSNumber
    let nCantidad: NSNumber
    let nCosto: NSNumber
    let nFactura: NSNumber
    let nPrecioUnitario: NSNumber
    let nRenglon: NSNumber
    let nTotal: NSNumber
    let nUtilidad: NSNumber
    let sArticulo: String
    
    init(bBloqueCosto: Bool, nAlmacen: NSNumber, nCaja: NSNumber, nCantidad: NSNumber, nCosto: NSNumber, nFactura: NSNumber, nPrecioUnitario: NSNumber, nRenglon: NSNumber, nTotal: NSNumber, nUtilidad: NSNumber, sArticulo: String) {
        self.bBloqueCosto = bBloqueCosto
        self.nAlmacen = nAlmacen
        self.nCaja = nCaja
        self.nFactura = nFactura
        self.nPrecioUnitario = nPrecioUnitario
        self.nRenglon = nRenglon
        self.nTotal = nTotal
        self.nUtilidad = nUtilidad
        self.sArticulo = sArticulo
        self.nCantidad = nCantidad
        self.nCosto = nCosto
    }
}
