//
//  Menu.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 25/11/14.
//  Copyright (c) 2014 Isaac. All rights reserved.
//

import UIKit

class Menu: UIViewController {
    
    @IBOutlet weak var scroll_view: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroll_view.scrollEnabled = true
        scroll_view.contentSize = CGSizeMake(scroll_view.frame.size.width, scroll_view.frame.size.height+10)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btn_antiguedad_de_saldos(sender: AnyObject) {
        var antiguedad : UINavigationController = self.storyboard?.instantiateViewControllerWithIdentifier("Antiguedad") as UINavigationController
        self.presentViewController(antiguedad, animated: true, completion: nil)
    }
    
    @IBAction func btn_mesa_ayuda(sender: AnyObject) {
        var mesa : UINavigationController = self.storyboard?.instantiateViewControllerWithIdentifier("Mesa_Ayuda") as UINavigationController
        self.presentViewController(mesa, animated: true, completion: nil)
    }
    
    @IBAction func btn_facturas_pressed(sender: AnyObject) {
        var factura : UINavigationController = self.storyboard?.instantiateViewControllerWithIdentifier("Facturas") as UINavigationController
        self.presentViewController(factura, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
