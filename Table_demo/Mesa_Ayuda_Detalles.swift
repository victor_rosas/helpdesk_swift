//
//  Mesa_Ayuda_Detalles.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 09/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class Mesa_Ayuda_Detalles: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var myTable: UITableView!
    var idTecnico : String =  ""
    var fecha_inicio: String = ""
    var fecha_final: String = ""
    
    var diccionarioMesa = NSMutableDictionary()
    let formatoHora = NSDateFormatter()
    
    var titulos = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatoHora.dateFormat = "HH:mm:ss"
        self.traerMesaDetalles()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return titulos.count
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

            let header = UIView();
            header.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0)
            header.backgroundColor = UIColor(red: 130/255, green: 171/255, blue: 198/255, alpha: 1.0)
            let label = UILabel();
            label.frame = CGRect(x: 2, y: 1, width: tableView.frame.size.width, height: 20)
            label.text = " " + titulos[section]
            label.font = UIFont(name: "Arial", size: 12.2)
            label.textColor = UIColor.whiteColor()
            header.addSubview(label)
            return header
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var llave = titulos[section]
        var arrMesa = diccionarioMesa.objectForKey(llave) as NSMutableDictionary
        return arrMesa.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let llave = titulos[indexPath.section]
        
        let arrMesa = diccionarioMesa.objectForKey(llave) as NSMutableDictionary
        
        var titulos_empresas = [String]()
        titulos_empresas = arrMesa.allKeys as [String]
        sort(&titulos_empresas)
        
        //TOTAL DE TIEMPO DEDICADO
        var llave_empresa = titulos_empresas[indexPath.row]
        var objeto_mesa = arrMesa.objectForKey(llave_empresa) as [Objeto_Mesa_Ayuda_Detalles]
        var total = 0
        
        for i in 0..<objeto_mesa.count
        {
            total += objeto_mesa[i].sMinutos
        }
        
        let cell: TC_Mesa_Ayuda_Detalles = tableView.dequeueReusableCellWithIdentifier("Cell_Empresas") as TC_Mesa_Ayuda_Detalles
        var total_double = Double(total)
        var total_horas = total_double/60.0
        cell.setCell(titulos_empresas[indexPath.row], horas: String(format:"%.2f", total_horas))
        return cell
    }
    
    @IBAction func back_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func traerMesaDetalles()
    {
        DataManager.establecerBusquedaMesa(idTecnico, inicio: fecha_inicio, fin: fecha_final)
        DataManager.traeMesaAyudaDetalles { (facturaData) -> Void in
            let json = JSON(data: facturaData)
            
            if let mesaArray = json["traeBusquedaMesaAyudaResult"].arrayValue {
                
                for mesaDict in mesaArray {
                    
                    var sTecnico: String = mesaDict["sTecnico"].stringValue!
                    var sCliente: String = mesaDict["sCliente"].stringValue!
                    var sTituloTicket: String = mesaDict["sTituloTicket"].stringValue!
                    var sFechaInicio: String = mesaDict["sHoraInicio"].stringValue!
                    var sFechaFin: String = mesaDict["sHoraFin"].stringValue!
                    var sFecha: String = mesaDict["sFecha"].stringValue!
                    
                    var horaInicio = self.formatoHora.dateFromString(sFechaInicio)
                    var horaFin = self.formatoHora.dateFromString(sFechaFin)
                    
                    let calendar = NSCalendar.currentCalendar()
                    let datecomponenets = calendar.components(NSCalendarUnit.CalendarUnitMinute, fromDate: horaInicio!, toDate: horaFin!, options: nil)
                    let minutos = datecomponenets.minute
                    
                    var mesa = Objeto_Mesa_Ayuda_Detalles(sTecnico: sTecnico, sCliente: sCliente, sTituloTicket: sTituloTicket, sFechaInicio: horaInicio!, sFechaFin: horaFin!, sFecha: sFecha, sMinutos: minutos)
                    
                    
                    //seccion de prueba
                    //var agrupado_por_sCliente = NSMutableDictionary()
                    //var arreglo_objetos = [Objeto_Mesa_Ayuda_Detalles]()
                    //if(self.diccionarioMesa)
                    
                    var diccionarioMesaEmpresa = NSMutableDictionary()
                    var arrMesaEmpresa = [Objeto_Mesa_Ayuda_Detalles]()
                    
                    if(self.diccionarioMesa.objectForKey(sTecnico)==nil)
                    {
                        arrMesaEmpresa.append(mesa)
                        diccionarioMesaEmpresa.setObject(arrMesaEmpresa, forKey: sCliente)
                        self.diccionarioMesa.setObject(diccionarioMesaEmpresa, forKey: sTecnico)
                    }
                    else
                    {
                        var arrTemporal = self.diccionarioMesa.objectForKey(sTecnico) as NSMutableDictionary
                        if(arrTemporal.objectForKey(sCliente)==nil)
                        {
                            arrMesaEmpresa.append(mesa)
                            arrTemporal.setObject(arrMesaEmpresa, forKey: sCliente)
                            self.diccionarioMesa.setObject(arrTemporal, forKey: sTecnico)
                        }
                        else
                        {
                            var arrTemporalEmpresa = arrTemporal.objectForKey(sCliente) as [Objeto_Mesa_Ayuda_Detalles]
                            arrTemporalEmpresa.append(mesa)
                            arrTemporal.setObject(arrTemporalEmpresa, forKey: sCliente)
                            self.diccionarioMesa.setObject(arrTemporal, forKey: sTecnico)
                        }
                    }
                    
                }
                
                self.titulos = self.diccionarioMesa.allKeys as [String]
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.myTable.reloadData()
                })
            }
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            let detail_view_controller:Mesa_Ayuda_Detalles_Empresa = segue.destinationViewController as Mesa_Ayuda_Detalles_Empresa
            let section = myTable.indexPathForSelectedRow()!.section
            let row = myTable.indexPathForSelectedRow()!.row
            
            var llave = titulos[section]
            var dicMesa = diccionarioMesa.objectForKey(llave) as NSMutableDictionary
            var titulos_empresas = [String]()
            titulos_empresas = dicMesa.allKeys as [String]
            sort(&titulos_empresas)
            var llave_empresa = titulos_empresas[row]
            var arrMesa = dicMesa.objectForKey(llave_empresa) as [Objeto_Mesa_Ayuda_Detalles]
            
            for i in 0..<arrMesa.count
            {
                detail_view_controller.arrayMesa.addObject(arrMesa[i])
            }
            detail_view_controller.title = llave_empresa
    }

}
