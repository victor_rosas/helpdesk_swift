//
//  VC_Facturas_Parametros.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 10/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class VC_Facturas_Parametros: UITableViewController {
    var selected_row = 0
    var fecha_inicio = NSDate()
    var fecha_final = NSDate()
    let formato_fecha = NSDateFormatter()
    let formato_fecha_string = NSDateFormatter()
    
    @IBOutlet weak var label_fecha_inicio: UILabel!
    @IBOutlet weak var label_fecha_final: UILabel!
    @IBOutlet weak var pickerView: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formato_fecha.dateFormat = "dd-MM-yyyy"
        formato_fecha_string.dateFormat = "dd/MM/yyyy"
        label_fecha_inicio.text = formato_fecha.stringFromDate(fecha_inicio)
        label_fecha_final.text = formato_fecha.stringFromDate(fecha_final)
    }
    
    override func viewWillAppear(animated: Bool) {
        var ip = NSIndexPath(forRow: 0, inSection: 0)
        self.tableView.selectRowAtIndexPath(ip, animated: true, scrollPosition: UITableViewScrollPosition.Bottom)
        selected_row=0
        pickerView.setDate(fecha_inicio, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func date_picker(sender: AnyObject) {
         escribir_fecha(sender.date!!)
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selected_row = indexPath.row
        if(selected_row == 0)
        {
            pickerView.setDate(fecha_inicio, animated: true)
        }
        else
        {
            pickerView.setDate(fecha_final, animated: true)
        }
    }
    
    func escribir_fecha(date:NSDate)
    {
        if(selected_row == 0)
        {
            fecha_inicio = date
            label_fecha_inicio.text = formato_fecha.stringFromDate(fecha_inicio)
        }
        else if(selected_row == 1)
        {
            fecha_final = date
            label_fecha_final.text = formato_fecha.stringFromDate(fecha_final)
        }
    }

    @IBAction func btn_back_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let siguiente:VC_Facturas_Factura = segue.destinationViewController as VC_Facturas_Factura
        
        siguiente.fecha_inicio = formato_fecha_string.stringFromDate(fecha_inicio)
        siguiente.fecha_final = formato_fecha_string.stringFromDate(fecha_final)
        
    }
}
