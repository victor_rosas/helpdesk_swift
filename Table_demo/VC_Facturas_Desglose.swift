//
//  VC_Facturas_Desglose.swift
//  Table_demo
//
//  Created by Miguel Banderas on 15/01/15.
//  Copyright (c) 2015 Aaron. All rights reserved.
//

import UIKit

class VC_Facturas_Desglose: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var myTableView: UITableView!
    var factura: NSNumber = 0
    var tipo: NSNumber = 0
    var almacen: NSNumber = 0
    var caja: NSNumber = 0
    var fecha: String = ""
    var arrayFacturas = NSMutableArray()
    var sortedArray = NSArray()
    
    let formato_moneda = NSNumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        formato_moneda.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        formato_moneda.locale = NSLocale(localeIdentifier: "es_MX")
        arrayFacturas.removeAllObjects()
        
        self.traeFacturaDetalle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: TC_Custom_Factura_Detalle = tableView.dequeueReusableCellWithIdentifier("Cell_Facturas_Detalle") as TC_Custom_Factura_Detalle
        let factura = sortedArray[indexPath.row] as OBJ_Factura_Detalle
        
        var precio_unitacio = self.formato_moneda.stringFromNumber(factura.nPrecioUnitario)
        precio_unitacio = precio_unitacio?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        var total = self.formato_moneda.stringFromNumber(factura.nTotal)
        total = total?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        var costo = self.formato_moneda.stringFromNumber(factura.nCosto)
        costo = costo?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        var utilidad = self.formato_moneda.stringFromNumber(factura.nUtilidad)
        utilidad = utilidad?.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        cell.setCell(factura.sArticulo, label_nPrecioUnitario: precio_unitacio!, label_nCantidad: factura.nCantidad.stringValue, label_nTotal: total!, label_nCosto: costo!, label_nUtilidad: utilidad!, candado: factura.bBloqueCosto)
        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func traeFacturaDetalle(){
        DataManager.establecerBusquedaFacturaDetalle(factura.stringValue, tipo: tipo.stringValue, almacen: almacen.stringValue, caja: caja.stringValue)
        DataManager.traerFacturasDetalle { (facturaData) -> Void in
            let json = JSON(data: facturaData)
            
            if let facturaArray = json["BuscaDetalleFacturaResult"].arrayValue {
                
                for facturaDic in facturaArray {
                    var bBloqueCosto: Bool = facturaDic["bBloqueCosto"].boolValue
                    var nAlmacen: NSNumber = facturaDic["nAlmacen"].integerValue!
                    var nCaja: NSNumber = facturaDic["nCaja"].integerValue!
                    var nCantidad: NSNumber = facturaDic["nCantidad"].integerValue!
                    var nCosto: NSNumber = facturaDic["nCosto"].integerValue!
                    var nFactura: NSNumber = facturaDic["nFactura"].integerValue!
                    var nPrecioUnitario: NSNumber = facturaDic["nPrecioUnitario"].integerValue!
                    var nRenglon: NSNumber = facturaDic["nRenglon"].integerValue!
                    var nTotal: NSNumber = facturaDic["nTotal"].integerValue!
                    var nUtilidad: NSNumber = facturaDic["nUtilidad"].integerValue!
                    var sArticulo: String = facturaDic["sArticulo"].stringValue!

                    var factura = OBJ_Factura_Detalle(bBloqueCosto: bBloqueCosto, nAlmacen: nAlmacen, nCaja: nCaja, nCantidad: nCantidad, nCosto: nCosto, nFactura: nFactura, nPrecioUnitario: nPrecioUnitario, nRenglon: nRenglon, nTotal: nTotal, nUtilidad: nUtilidad, sArticulo: sArticulo)
                    self.arrayFacturas.addObject(factura)
                }
                
                var sorter = NSSortDescriptor(key: "nRenglon", ascending: true)
                self.sortedArray = (self.arrayFacturas as NSArray).sortedArrayUsingDescriptors([sorter])
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.myTableView.reloadData()
                })
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let siguiente:VC_Facturas_Detalle = segue.destinationViewController as VC_Facturas_Detalle
        var factura : OBJ_Factura_Detalle
        let row = myTableView.indexPathForSelectedRow()!.row
        factura = sortedArray[row] as OBJ_Factura_Detalle
        
        siguiente.title = factura.sArticulo
        siguiente.total = factura.nTotal
        siguiente.costo = factura.nCosto
        
        siguiente.factura = factura.nFactura
        siguiente.almacen = factura.nAlmacen
        siguiente.caja = factura.nCaja
        siguiente.renglon = factura.nRenglon
        siguiente.tipo = tipo
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if identifier == "Facturas_Detalle" {
            var factura : OBJ_Factura_Detalle
            let row = myTableView.indexPathForSelectedRow()!.row
            factura = sortedArray[row] as OBJ_Factura_Detalle
            
            if (factura.bBloqueCosto) {
                let alert = UIAlertView()
                alert.title = "Bloqueado"
                alert.message = "La modificación de este artículo está bloqueada."
                alert.addButtonWithTitle("Ok")
                alert.show()
                return false
            }
                
            else {
                return true
            }
        }
        return true
    }
    
    @IBAction func home_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
