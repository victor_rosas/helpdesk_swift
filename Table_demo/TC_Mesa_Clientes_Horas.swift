//
//  TC_Mesa_Clientes_Horas.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 11/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class TC_Mesa_Clientes_Horas: UITableViewCell {

    @IBOutlet weak var label_tecnico: UILabel!
    @IBOutlet weak var label_horas: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(tecnico: String, horas: String)
    {
        self.label_tecnico.text = tecnico
        self.label_horas.text = horas
    }

}
