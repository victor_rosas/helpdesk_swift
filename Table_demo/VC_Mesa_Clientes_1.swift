//
//  VC_Mesa_Clientes_1.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 11/12/14.
//  Copyright (c) 2014 Aaron. All rights reserved.
//

import UIKit

class VC_Mesa_Clientes_1: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var agrupado_por_cliente = NSMutableDictionary()
    var idTecnico : String =  ""
    let formatoHora = NSDateFormatter()
    var titulos = [String]()
    @IBOutlet weak var myTable: UITableView!
    
    //Fechas
    var fecha_inicio: String = ""
    var fecha_final: String = ""
    var cliente: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        formatoHora.dateFormat = "HH:mm:ss"
        self.traerMesaDetalles()
        // Do any additional setup after loading the view.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.titulos.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var llave = titulos[section]
        var agrupado_tecnicos = self.agrupado_por_cliente.objectForKey(llave) as NSMutableDictionary
        return agrupado_tecnicos.count
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = UIView();
        header.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0)
        header.backgroundColor = UIColor(red: 130/255, green: 171/255, blue: 198/255, alpha: 1.0)
        let label = UILabel();
        label.frame = CGRect(x: 2, y: 1, width: tableView.frame.size.width, height: 20)
        label.text = " " + titulos[section]
        label.font = UIFont(name: "Arial", size: 12.2)
        label.textColor = UIColor.whiteColor()
        header.addSubview(label)
        return header
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let llave = titulos[indexPath.section]
        
        let diccionario_tecnicos = self.agrupado_por_cliente.objectForKey(llave) as NSMutableDictionary
        
        var titulos_tecnicos = [String]()
        titulos_tecnicos = diccionario_tecnicos.allKeys as [String]
        sort(&titulos_tecnicos)
        
        //TOTAL DE TIEMPO DEDICADO
        var llave_empresa = titulos_tecnicos[indexPath.row]
        var arreglo_objeto = diccionario_tecnicos.objectForKey(llave_empresa) as [Objeto_Mesa_Ayuda_Detalles]
        var total = 0
        
        for i in 0..<arreglo_objeto.count
        {
            total += arreglo_objeto[i].sMinutos
        }
        
        let cell: TC_Mesa_Clientes_Horas = tableView.dequeueReusableCellWithIdentifier("Cell_Mesa_Clientes") as TC_Mesa_Clientes_Horas
        var total_double = Double(total)
        var total_horas = total_double/60.0
        cell.setCell(titulos_tecnicos[indexPath.row], horas: String(format:"%.2f", total_horas))
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func traerMesaDetalles()
    {
        DataManager.establecerBusquedaMesa(idTecnico, inicio: fecha_inicio, fin: fecha_final)
        DataManager.traeMesaAyudaDetalles { (facturaData) -> Void in
            let json = JSON(data: facturaData)
            
            if let mesaArray = json["traeBusquedaMesaAyudaResult"].arrayValue {
                
                for mesaDict in mesaArray {
                    
                    var sTecnico: String = mesaDict["sTecnico"].stringValue!
                    var sCliente: String = mesaDict["sCliente"].stringValue!
                    var sTituloTicket: String = mesaDict["sTituloTicket"].stringValue!
                    var sFechaInicio: String = mesaDict["sHoraInicio"].stringValue!
                    var sFechaFin: String = mesaDict["sHoraFin"].stringValue!
                    var sFecha: String = mesaDict["sFecha"].stringValue!
                    
                    if(sCliente == self.cliente || self.cliente == "Ninguno")
                    {
                        var horaInicio = self.formatoHora.dateFromString(sFechaInicio)
                        var horaFin = self.formatoHora.dateFromString(sFechaFin)
                        
                        let calendar = NSCalendar.currentCalendar()
                        let datecomponenets = calendar.components(NSCalendarUnit.CalendarUnitMinute, fromDate: horaInicio!, toDate: horaFin!, options: nil)
                        let minutos = datecomponenets.minute
                        
                        var mesa = Objeto_Mesa_Ayuda_Detalles(sTecnico: sTecnico, sCliente: sCliente, sTituloTicket: sTituloTicket, sFechaInicio: horaInicio!, sFechaFin: horaFin!, sFecha: sFecha, sMinutos: minutos)
                        
                        var agrupado_por_Tecnico = NSMutableDictionary()
                        var agrupado_por_Tarea = NSMutableDictionary()
                        var arreglo_objetos = [Objeto_Mesa_Ayuda_Detalles]()
                        if(self.agrupado_por_cliente.objectForKey(sCliente)==nil)
                        {
                            arreglo_objetos.append(mesa)
                            agrupado_por_Tecnico.setObject(arreglo_objetos, forKey: sTecnico)
                            self.agrupado_por_cliente.setObject(agrupado_por_Tecnico, forKey: sCliente)
                        }
                        else
                        {
                            var agrupado_tecnico_existe = self.agrupado_por_cliente.objectForKey(sCliente) as NSMutableDictionary
                            
                            if(agrupado_tecnico_existe.objectForKey(sTecnico)==nil)
                            {
                                arreglo_objetos.append(mesa)
                                agrupado_tecnico_existe.setObject(arreglo_objetos, forKey: sTecnico)
                                self.agrupado_por_cliente.setObject(agrupado_tecnico_existe, forKey: sCliente)
                            }
                            else
                            {
                                var arreglo_existente = agrupado_tecnico_existe.objectForKey(sTecnico) as [Objeto_Mesa_Ayuda_Detalles]
                                arreglo_existente.append(mesa)
                                agrupado_tecnico_existe.setObject(arreglo_existente, forKey: sTecnico)
                                self.agrupado_por_cliente.setObject(agrupado_tecnico_existe, forKey: sCliente)
                            }
                        }
                    }
                   
                }
                
                self.titulos = self.agrupado_por_cliente.allKeys as [String]
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.myTable.reloadData()
                })
            }
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let detail_view_controller:VC_Mesa_Clientes_2 = segue.destinationViewController as VC_Mesa_Clientes_2
        let section = myTable.indexPathForSelectedRow()!.section
        let row = myTable.indexPathForSelectedRow()!.row
        
        var llave = titulos[section]
        var dicMesa = self.agrupado_por_cliente.objectForKey(llave) as NSMutableDictionary
        var titulos_empresas = [String]()
        titulos_empresas = dicMesa.allKeys as [String]
        sort(&titulos_empresas)
        var llave_empresa = titulos_empresas[row]
        var arrMesa = dicMesa.objectForKey(llave_empresa) as [Objeto_Mesa_Ayuda_Detalles]
        
        for i in 0..<arrMesa.count
        {
            detail_view_controller.arrayMesa.addObject(arrMesa[i])
        }
        detail_view_controller.title = llave_empresa
    }
    
    @IBAction func btn_home_pressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //POR SI ALGUNA VEZ SE NECESITA, CALCULADO DE RANGO
    /*func creacion_de_intervalos_de_fecha()
    {
        self.fecha_dia = self.dateString.dateFromString(self.fecha_inicio)!
        
        let now = self.fecha_dia
        var startDate: NSDate? = nil
        var duration: NSTimeInterval = 0
        let cal = NSCalendar.currentCalendar()
        
        cal.rangeOfUnit(NSCalendarUnit.MonthCalendarUnit, startDate: &startDate,
            interval: &duration, forDate: now)
        
        self.fecha_inicio_mes = startDate!
        self.fecha_final_mes = fecha_inicio_mes.dateByAddingTimeInterval(duration-1)
        
        debugPrint(self.fecha_dia)
        debugPrint(self.fecha_inicio_mes)
        debugPrint(self.fecha_final_mes)
    }*/

}
