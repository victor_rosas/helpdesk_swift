//
//  CustomCell.swift
//  Table_demo
//
//  Created by Ezequiel Melendez on 12/11/14.
//  Copyright (c) 2014 Isaac. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var label_sReferencia: UILabel!
    @IBOutlet weak var label_dVen: UILabel!
    @IBOutlet weak var label_dDias: UILabel!
    @IBOutlet weak var label_sFechaMovimiento: UILabel!
    @IBOutlet weak var label_sNombre: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(label_sReferencias: String, label_dVen: String, label_dDias: String, label_sFechaMovimiento: String, label_sNombre: String, label_show: Bool)
    {
        self.label_sNombre.text = label_sNombre
        self.label_sReferencia.text = label_sReferencias
        self.label_dVen.text = label_dVen
        self.label_dDias.text = "" + label_dDias
        self.label_sFechaMovimiento.text = label_sFechaMovimiento
        
        self.label_sFechaMovimiento.layer.cornerRadius = 5
        self.label_sFechaMovimiento.layer.masksToBounds = true
        
        if(label_show)
        {
            self.label_sReferencia.frame.origin.y = 10
            self.label_dVen.frame.origin.y = 10
            self.label_dDias.frame.origin.y = 10
            self.label_sFechaMovimiento.frame.origin.y = 10
        }
        else
        {
            self.label_sReferencia.frame.origin.y = 25
            self.label_dVen.frame.origin.y = 25
            self.label_dDias.frame.origin.y = 25
            self.label_sFechaMovimiento.frame.origin.y = 25
        }
        self.label_sNombre.hidden = label_show
        //self.myImage.image = UIImage(named:name)
    }

}
